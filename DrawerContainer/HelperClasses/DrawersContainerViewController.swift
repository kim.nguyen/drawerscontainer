//
//  DrawersContainerViewController.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 03.01.20.
//  Copyright © 2020 Neofonie Mobile. All rights reserved.
//

import UIKit

/// An UIViewController class with pass through background responsible for manipulating drawers
public class DrawersContainerViewController: UIViewController {
    
    public var defaultBottomDrawerHeight: CGFloat = max(UIScreen.main.bounds.height, UIScreen.main.bounds.width) / 3
    public var defaultTopDrawerHeight: CGFloat = 0.0
    
    private var leftMargin: CGFloat = 0.0
    private var bottomMargin: CGFloat = 0.0
    private var topMargin: CGFloat = 0.0
    
    public var topDrawer: DrawerViewController?
    public var bottomDrawer: DrawerViewController?
    
    private var topContainer: UIView?
    private var bottomContainer: UIView?
    
    private var drawerWidth: CGFloat = min(UIScreen.main.bounds.height, UIScreen.main.bounds.width)
    private var drawerWidthConstraint = NSLayoutConstraint()
    
    private var topDrawerHeight: CGFloat = 0.0
    private var topDrawerHeightConstraint = NSLayoutConstraint()
    
    private var bottomDrawerHeight: CGFloat = 0.0
    private var bottomDrawerHeightConstraint = NSLayoutConstraint()
    
    init(topDrawer: DrawerViewController? = nil, bottomDrawer: DrawerViewController?) {
        self.topDrawer = topDrawer
        self.bottomDrawer = bottomDrawer
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func loadView() {
        super.loadView()
        view.backgroundColor = .clear
        
        let passthroughView = PassthroughView()
        passthroughView.backgroundColor = .clear
        passthroughView.touchDelegate = presentingViewController?.view
        
        passthroughView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(passthroughView)
        
        NSLayoutConstraint.activate([
            passthroughView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            passthroughView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            passthroughView.topAnchor.constraint(equalTo: view.topAnchor),
            passthroughView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupDefaultSizes()
        setupDrawers()
    }
    
    private func drawerContainer(for viewController: UIViewController) -> UIView {
        // Embed a viewControoler into a container view
        let containerView = UIView(frame: CGRect.zero)
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(viewController.view)
        NSLayoutConstraint.activate([
            viewController.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            viewController.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            viewController.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            viewController.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
        
        return containerView
    }
    
    private func setupDefaultSizes() {
        // custom sizes depending on device
        if UIDevice.current.userInterfaceIdiom == .phone {
            defaultBottomDrawerHeight = max(UIScreen.main.bounds.height, UIScreen.main.bounds.width) / 3
            defaultTopDrawerHeight = 130
            drawerWidth = min(UIScreen.main.bounds.height, UIScreen.main.bounds.width)
            leftMargin = 0.0
            bottomMargin = 0.0
            topMargin = 0.0
        } else {
            // for iPad and other
            defaultBottomDrawerHeight = 400
            defaultTopDrawerHeight = 130
            drawerWidth = 400
            leftMargin = 40
            bottomMargin = 40
            topMargin = 20
        }
    }
    
    private func setupTopDrawer(_ drawer: UIViewController) {
        self.addChild(drawer)
        topContainer = drawerContainer(for: drawer)
        if let container = topContainer {
            container.layer.cornerRadius = 8
            container.layer.maskedCorners = (topMargin == 0) ? [.layerMaxXMaxYCorner, .layerMinXMaxYCorner] : [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMinYCorner]
            container.clipsToBounds = true
            container.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(container)
            topDrawerHeightConstraint = container.heightAnchor.constraint(equalToConstant: defaultTopDrawerHeight)
            drawerWidthConstraint = container.widthAnchor.constraint(equalToConstant: drawerWidth)
            NSLayoutConstraint.activate([
                container.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: leftMargin),
                drawerWidthConstraint,
                container.topAnchor.constraint(equalTo: self.view.topAnchor, constant: topMargin),
                topDrawerHeightConstraint
            ])
        }
        
        // Add pan gesture recognizer
        let topContainerPanReconginzer = UIPanGestureRecognizer(target: self, action: #selector(handleTopPanRecognizer(_:)))
        topContainer?.addGestureRecognizer(topContainerPanReconginzer)
        
        drawer.didMove(toParent: self)
    }
    
    private func setupBottomDrawer(_ drawer: UIViewController) {
        self.addChild(drawer)
        
        bottomContainer = drawerContainer(for: drawer)
        if let container = bottomContainer {
            container.layer.cornerRadius = 8
            container.layer.maskedCorners = (bottomMargin == 0) ? [.layerMinXMinYCorner, .layerMaxXMinYCorner] : [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMinYCorner]
            container.clipsToBounds = true
            container.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(container)
            bottomDrawerHeightConstraint = container.heightAnchor.constraint(equalToConstant: defaultBottomDrawerHeight)
            drawerWidthConstraint = container.widthAnchor.constraint(equalToConstant: drawerWidth)
            NSLayoutConstraint.activate([
                container.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: leftMargin),
                drawerWidthConstraint,
                container.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -bottomMargin),
                bottomDrawerHeightConstraint
            ])
        }

        // Add pan gesture recognizer
        let bottomContainerPanReconginzer = UIPanGestureRecognizer(target: self, action: #selector(handleBottomPanRecognizer(_:)))
        bottomContainer?.addGestureRecognizer(bottomContainerPanReconginzer)
        
        drawer.didMove(toParent: self)
    }
    
    func maximizeBottomDrawer() {
        DispatchQueue.main.async {
            self.bottomDrawerHeightConstraint.constant = self.view.bounds.height - self.topDrawerHeightConstraint.constant - self.bottomMargin - self.topMargin
            self.bottomDrawer?.didMaximize()
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func normalizeBottomDrawer() {
        DispatchQueue.main.async {
            self.bottomDrawerHeightConstraint.constant = self.defaultBottomDrawerHeight
            self.bottomDrawer?.didNormalize()
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func maximizeTopDrawer() {
        DispatchQueue.main.async {
            self.topDrawerHeightConstraint.constant = self.view.bounds.height - self.bottomDrawerHeightConstraint.constant - self.topMargin - self.bottomMargin
            self.topDrawer?.didMaximize()
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func normalizeTopDrawer() {
        DispatchQueue.main.async {
            self.topDrawerHeightConstraint.constant = self.defaultTopDrawerHeight
            self.topDrawer?.didNormalize()
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    public func setupDrawers() {
        if let topDrawer = self.topDrawer {
            defaultTopDrawerHeight = 130
            setupTopDrawer(topDrawer)
        }
        
        if let bottomDrawer = self.bottomDrawer {
            setupBottomDrawer(bottomDrawer)
        }
    }
    
    @objc func handleBottomPanRecognizer(_ sender: UIPanGestureRecognizer) {
        guard sender.view != nil else { return }
        
        switch sender.state {
        case .began:
            bottomDrawerHeight = bottomDrawerHeightConstraint.constant
            
        case .ended:
            // thresholdY is the Y postion where the drawer will go up to the top or back to the standard height
            let thresholdY = (UIScreen.main.bounds.height - defaultTopDrawerHeight + defaultBottomDrawerHeight) / 2
            if bottomDrawerHeightConstraint.constant > thresholdY {
                maximizeBottomDrawer()
            } else {
                normalizeBottomDrawer()
            }
            
        default:
            let translation = sender.translation(in: self.view)
            let newConstraint = bottomDrawerHeight - translation.y
            bottomDrawerHeightConstraint.constant = newConstraint
        }
    }
    
    @objc func handleTopPanRecognizer(_ sender: UIPanGestureRecognizer) {
        guard sender.view != nil else { return }
        
        switch sender.state {
        case .began:
            topDrawerHeight = topDrawerHeightConstraint.constant
            
        case .ended:
            let thresholdY = (UIScreen.main.bounds.height - defaultBottomDrawerHeight + defaultTopDrawerHeight) / 2
            if topDrawerHeightConstraint.constant > thresholdY {
                maximizeTopDrawer()
            } else {
                normalizeTopDrawer()
            }
            
        default:
            let translation = sender.translation(in: self.view)
            let newConstraint = topDrawerHeight + translation.y
            topDrawerHeightConstraint.constant = newConstraint
        }
    }
}
