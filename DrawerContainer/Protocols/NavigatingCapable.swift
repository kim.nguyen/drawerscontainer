//
//  NavigatingCapable.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 03.01.20.
//  Copyright © 2020 Neofonie Mobile. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

public protocol NavigatingCapable {
    var icon: UIImage { get set }
    var name: String { get set }
    var address: String { get set }
    var location: CLLocation { get set }
}
