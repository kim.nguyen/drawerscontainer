//
//  Coordinator.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 31.12.19.
//  Copyright © 2019 Neofonie Mobile. All rights reserved.
//

import UIKit
import Foundation

public protocol Coordination: class {
    var childCoordinators: [Coordination] { get set }
    var presenter: UIViewController { get set }
    var viewControllers: [UIViewController] { get set }
    
    func start()
    /// Present a viewController at a certain layer level
    /// Cascade = true : present on top existing viewControllers in the stack
    /// Cascase = false : dismiss all viewControollers then present
    func present(_ viewController: UIViewController, cascade: Bool)
}

extension Coordination {
    func present(_ viewController: UIViewController, cascade: Bool = false) {
        viewController.modalPresentationStyle = .overCurrentContext
        guard let firstViewController = viewControllers.first else { return }
        guard let lastViewController = viewControllers.last else { return }
        
        if cascade {
            presenter = lastViewController
            
            let viewControlersToBeNormalized = viewControllers.filter { $0 is DrawersContainerViewController}
            viewControlersToBeNormalized.forEach { ($0 as! DrawersContainerViewController).normalizeBottomDrawer()
            }
            
            presenter.present(viewController, animated: true) {
                self.viewControllers.append(viewController)
                self.presenter = viewController
            }
        } else {
            // dismiss all top viewControllers
            presenter = firstViewController
            let viewControllersToBeRemoved = viewControllers.filter { $0 !== presenter }
            viewControllers = [presenter]

            // Present only when all overlayed viewControllers are dismissed
            dismissViewControllers(viewControllersToBeRemoved) {
                DispatchQueue.main.async {
                    self.presenter.present(viewController, animated: true) {
                        self.viewControllers.append(viewController)
                        self.presenter = viewController
                    }
                }
            }
        }
    }
    
    private func dismissViewControllers(_ viewControllers: [UIViewController], completion: @escaping () -> Void) {
        guard !viewControllers.isEmpty else {
            completion()
            return
        }
        var proccessingVCs = viewControllers
        if let viewController = proccessingVCs.last {
            DispatchQueue.main.async {
                viewController.dismiss(animated: false) {
                    proccessingVCs.removeLast()
                    self.dismissViewControllers(proccessingVCs, completion: completion)
                }
            }
        }
    }
}
