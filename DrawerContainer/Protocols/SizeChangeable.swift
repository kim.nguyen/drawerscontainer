//
//  SizeChangeable.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 03.01.20.
//  Copyright © 2020 Neofonie Mobile. All rights reserved.
//

import Foundation
import UIKit

/// Protocol for drawers whose sizes are changeable between max and normal size
public protocol SizeChangeable: class {
    func didMaximize()
    func didNormalize()
}

public typealias DrawerViewController = UIViewController & SizeChangeable

