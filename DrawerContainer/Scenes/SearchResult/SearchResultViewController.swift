//
//  SearchResultViewController.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 30.12.19.
//  Copyright © 2019 Neofonie Mobile. All rights reserved.
//

import UIKit
import CoreLocation

public protocol SearchResultViewControllerDelegate: class {
    func searchResultViewControllerDidSelectRoute(to poi: NavigatingCapable)
    func searchResultViewControllerDidTapCloseButton()
}

class SearchResultViewController: DrawerViewController {
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    private var poiItem: POIItem?
    weak var delegate: SearchResultViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // mock data
        poiItem = POIItem(icon: UIImage(), name: "Mocked Name", address: "Mocked Address", location: CLLocation(latitude: 0, longitude: 0))
    }
    
    @IBAction func routeButtonTapped(_ sender: UIButton) {
        guard let poi = self.poiItem else { return }
        delegate?.searchResultViewControllerDidSelectRoute(to: poi)
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        delegate?.searchResultViewControllerDidTapCloseButton()
    }

    public func setup(with poi: NavigatingCapable) {
        poiItem = POIItem(icon: poi.icon, name: poi.name, address: poi.address, location: poi.location)
        setUI(with: poi)
    }
    
    private func setUI(with item: NavigatingCapable) {
        self.loadViewIfNeeded()
        titleLabel.text = item.name
        detailsLabel.text = item.address
        iconImageView.image = item.icon
    }
    
    func didMaximize() {
        closeButton.isHidden = false
        // Do something else
    }
    
    func didNormalize() {
        closeButton.isHidden = true
        // Do something else
    }
    
    struct POIItem: NavigatingCapable {
        var icon: UIImage
        var name: String
        var address: String
        var location: CLLocation
    }
}
