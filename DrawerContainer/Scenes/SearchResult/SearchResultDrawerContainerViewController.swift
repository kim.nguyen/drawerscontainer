//
//  SearchResultDrawerContainerViewController.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 03.01.20.
//  Copyright © 2020 Neofonie Mobile. All rights reserved.
//

import UIKit

protocol SearchResultDrawerContainerDelegate: class {
    func searchResultViewControllerDidSelectRoute(to poi: NavigatingCapable)
}

class SearchResultDrawerContainerViewController: DrawersContainerViewController {
    
    weak var delegate: SearchResultDrawerContainerDelegate?
    private let searchResultDrawer = UIStoryboard(name: "SearchResult", bundle: nil).instantiateInitialViewController() as! SearchResultViewController
    
    init(delegate: SearchResultDrawerContainerDelegate?) {
        self.delegate = delegate
        super.init(bottomDrawer: searchResultDrawer)
        searchResultDrawer.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupSearchResult(with poiItem: NavigatingCapable) {
        searchResultDrawer.setup(with: poiItem)
    }
}

extension SearchResultDrawerContainerViewController: SearchResultViewControllerDelegate {
    func searchResultViewControllerDidTapCloseButton() {
        normalizeBottomDrawer()
    }
    
    func searchResultViewControllerDidSelectRoute(to poi: NavigatingCapable) {
        delegate?.searchResultViewControllerDidSelectRoute(to: poi)
    }
}
