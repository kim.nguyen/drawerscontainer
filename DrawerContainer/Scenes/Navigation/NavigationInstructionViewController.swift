//
//  NavigationInstructionViewController.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 30.12.19.
//  Copyright © 2019 Neofonie Mobile. All rights reserved.
//

import UIKit

public protocol NavigationInstructionViewControllerDelegate: class {
    
}

class NavigationInstructionViewController: DrawerViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func didMaximize() {
        // Do something
    }
    
    func didNormalize() {
        // Do something
    }
}
