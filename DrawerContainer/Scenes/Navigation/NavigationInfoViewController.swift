//
//  NavigationInfoViewController.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 01.01.20.
//  Copyright © 2020 Neofonie Mobile. All rights reserved.
//

import UIKit

public protocol NavigationInfoViewControllerDelegate: class {
    func navigationControllerDidTapCloseButton()
}

class NavigationInfoViewController: DrawerViewController {
    
    weak var delegate: NavigationInfoViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        delegate?.navigationControllerDidTapCloseButton()
    }
    
    func didMaximize() {
        // Do something
    }
    
    func didNormalize() {
        // Do something
    }
}
