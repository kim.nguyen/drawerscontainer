//
//  NavigationDrawerContainerViewController.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 03.01.20.
//  Copyright © 2020 Neofonie Mobile. All rights reserved.
//

import UIKit

protocol NavigationDrawerContainerDelegate: class {
    func navigationControllerDidTapCloseButton()
}

class NavigationDrawerContainerViewController: DrawersContainerViewController, NavigationInfoViewControllerDelegate {
    
    weak var delegate: NavigationDrawerContainerDelegate?
    
    init(delegate: NavigationDrawerContainerDelegate?) {
        self.delegate = delegate
        let navigationInstructionDrawer = UIStoryboard(name: "NavigationInstruction", bundle: nil).instantiateInitialViewController() as! NavigationInstructionViewController
        let navigationInfoDrawer = UIStoryboard(name: "NavigationInfo", bundle: nil).instantiateInitialViewController() as! NavigationInfoViewController
        super.init(topDrawer: navigationInstructionDrawer, bottomDrawer: navigationInfoDrawer)
        navigationInfoDrawer.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func navigationControllerDidTapCloseButton() {
        delegate?.navigationControllerDidTapCloseButton()
    }
    
}
