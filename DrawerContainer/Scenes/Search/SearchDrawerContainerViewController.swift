//
//  SearchDrawerContainerViewController.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 03.01.20.
//  Copyright © 2020 Neofonie Mobile. All rights reserved.
//

import UIKit

// MARK: SearchDrawerContainer
protocol SearchDrawerContainerDelegate: class {
    func searchViewControllerDidSelect(item: NavigatingCapable)
}

class SearchDrawerContainerViewController: DrawersContainerViewController {
    private let searchDrawer = UIStoryboard(name: "Search", bundle: nil).instantiateInitialViewController() as! SearchViewController
    
    weak var delegate: SearchDrawerContainerDelegate?
    
    init(delegate: SearchDrawerContainerDelegate?) {
        self.delegate = delegate
        super.init(bottomDrawer: searchDrawer)
        searchDrawer.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension SearchDrawerContainerViewController: SearchViewControllerDelegate {
    func searchViewControllerTextFieldBeginEditing() {
        maximizeBottomDrawer()
    }
    
    func searchViewControllerDidSelect(item: NavigatingCapable) {
        delegate?.searchViewControllerDidSelect(item: item)
    }
    
    func searchViewControllerDidTapCloseButton() {
        normalizeBottomDrawer()
    }
}
