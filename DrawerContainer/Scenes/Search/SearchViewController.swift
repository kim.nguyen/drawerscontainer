//
//  SearchViewController.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 30.12.19.
//  Copyright © 2019 Neofonie Mobile. All rights reserved.
//

import UIKit
import CoreLocation

protocol SearchViewControllerDelegate: class {
    func searchViewControllerDidSelect(item: NavigatingCapable)
    func searchViewControllerDidTapCloseButton()
    func searchViewControllerTextFieldBeginEditing()
}

class SearchViewController: DrawerViewController {
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var closeButton: UIButton!

    weak var delegate: SearchViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func itemTapped(_ sender: UIButton) {
        // mock data
        let selectedItem = SearchResultItem(icon: UIImage(named: "mapMarker") ?? UIImage(), name: "Robert Koch Platz 4", address: "10115, Berlin, Germany", location: CLLocation(latitude: 0, longitude: 0))
        delegate?.searchViewControllerDidSelect(item: selectedItem)
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        delegate?.searchViewControllerDidTapCloseButton()
    }
    
    public func didMaximize() {
        closeButton.isHidden = false
        searchTextField.becomeFirstResponder()
    }
    
    public func didNormalize() {
        closeButton.isHidden = true
        searchTextField.text = nil
        searchTextField.resignFirstResponder()
    }
    
    struct SearchResultItem: NavigatingCapable {
        var icon: UIImage
        var name: String
        var address: String
        var location: CLLocation
    }
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        delegate?.searchViewControllerTextFieldBeginEditing()
        return true
    }
}
