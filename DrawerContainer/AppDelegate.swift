//
//  AppDelegate.swift
//  DrawerContainer
//
//  Created by Kim on 19.12.19.
//  Copyright © 2019 Neofonie Mobile. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var mainCoordinator: MainCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let rootViewController = UIViewController()
        rootViewController.view.backgroundColor = .white
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
        
        mainCoordinator = MainCoordinator(presenter: rootViewController)
        mainCoordinator?.start()
        
        return true
    }
}

