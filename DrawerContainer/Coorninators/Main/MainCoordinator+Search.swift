//
//  MainCoordinator+Search.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 03.01.20.
//  Copyright © 2020 Neofonie Mobile. All rights reserved.
//

import Foundation

extension MainCoordinator: SearchDrawerContainerDelegate {
    func searchViewControllerDidSelect(item: NavigatingCapable) {
        let searchResultDrawerContainer = SearchResultDrawerContainerViewController(delegate: self)
        searchResultDrawerContainer.setupSearchResult(with: item)
        // present searchResultViewController on top searchViewController
        
        DispatchQueue.main.async {
            self.present(searchResultDrawerContainer, cascade: true)
        }
    }
    
}
