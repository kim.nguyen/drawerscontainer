//
//  MainCoordinator+SearchResult.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 03.01.20.
//  Copyright © 2020 Neofonie Mobile. All rights reserved.
//

import Foundation

extension MainCoordinator: SearchResultDrawerContainerDelegate {
    func searchResultViewControllerDidSelectRoute(to poi: NavigatingCapable) {
        
        let navigationDrawerContainer = NavigationDrawerContainerViewController(delegate: self)
        
        // present navigationInfoViewController removing other viewControllers by default
        DispatchQueue.main.async {
            self.present(navigationDrawerContainer, cascade: false)
        }
    }
}
