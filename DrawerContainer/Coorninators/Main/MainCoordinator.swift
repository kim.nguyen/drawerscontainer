//
//  MainCoordinator.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 01.01.20.
//  Copyright © 2020 Neofonie Mobile. All rights reserved.
//

import Foundation
import UIKit

class MainCoordinator: Coordination {
    var viewControllers: [UIViewController] = []
    var childCoordinators: [Coordination] = []
    var presenter: UIViewController
    
    init(presenter: UIViewController) {
        self.presenter = presenter
    }
    
    func start() {
        let homeViewController = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController() as! HomeViewController

        homeViewController.modalPresentationStyle = .overCurrentContext
        
        DispatchQueue.main.async {
            self.presenter.present(homeViewController, animated: true) {
                self.viewControllers.append(homeViewController)
                self.presenter = homeViewController
                
                let searchDrawerContainer = SearchDrawerContainerViewController(delegate: self)
                // present SearchDrawer on Top homeScreen
                self.present(searchDrawerContainer, cascade: true)
            }
        }
    }
}

