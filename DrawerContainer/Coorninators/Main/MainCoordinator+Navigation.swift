//
//  MainCoordinator+Navigation.swift
//  DrawerContainer
//
//  Created by Kim Nguyen on 03.01.20.
//  Copyright © 2020 Neofonie Mobile. All rights reserved.
//

import Foundation

extension MainCoordinator: NavigationDrawerContainerDelegate {
    func navigationControllerDidTapCloseButton() {
        // present Search again
        let searchDrawerContainer = SearchDrawerContainerViewController(delegate: self)
        // present SearchDrawer on Top homeScreen
        DispatchQueue.main.async {
            self.present(searchDrawerContainer)
        }
    }
    
}
